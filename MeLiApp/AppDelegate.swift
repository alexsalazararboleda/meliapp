//
//  AppDelegate.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow()
        guard let window = self.window else { return false }
        appStartWithCurrentWindow()

        AppearanceHelper.setup()

        UIApplication.shared.delegate = self
        window.frame = UIScreen.main.bounds
        window.makeKeyAndVisible()

        return true
    }

    private func appStartWithCurrentWindow() {
        guard let window = self.window else { return }
        appCoordinator = AppCoordinator(window: window)
        appCoordinator.start()
    }
}


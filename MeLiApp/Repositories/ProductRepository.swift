//
//  ProductsRepository.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//

import Foundation
import Combine

protocol IProductRepository {
    func searchProductsBy(query: String, completion: @escaping (Result<ProductList, Error>) -> ())
}

class ProductRepository: IProductRepository {
    public init() {}
    private let productAPI = ProductAPI()
    
    public func searchProductsBy(query: String, completion: @escaping (Result<ProductList, Error>) -> ()) {
        productAPI.searchProductsBy(query: query, completion: completion)
    }
}

//
//  HomeCoordinator.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import UIKit

protocol HomeCoordinatorDelegate: AnyObject {}

class HomeCoordinator: Coordinator {

    // MARK: - Private properties
    fileprivate var coordinators: CoordinatorsDictionary
    var navigationController: UINavigationController {
        if let navigationController = rootViewController as? UINavigationController {
            return navigationController
        }
        return UINavigationController()
    }

    // MARK: - Properties
    var rootViewController: UIViewController
    weak var delegate: HomeCoordinatorDelegate?

    // MARK: - Initializer
    init() {
        rootViewController = UINavigationController()
        coordinators = [:]
    }

    func start() {
        showHomeScreen()
    }

    fileprivate func showHomeScreen() {
        let productRepository = ProductRepository()
        let viewModel: ProductListViewModel = {
            $0.coordinatorDelegate = self
            return $0
        }(ProductListViewModel(productRepository))
        let viewController = ProductListViewController(viewModel: viewModel)
        navigationController.setViewControllers([viewController], animated: true)
    }

    fileprivate func showDetailFor(_ product: Product) {
        let viewModel: ProductDetailViewModel = {
            $0.coordinatorDelegate = self
            return $0
        }(ProductDetailViewModel(product: product))
        let viewController = ProductDetailViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: true)
    }

    fileprivate func closeDetail() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}

extension HomeCoordinator: ProductListViewModelCoordinatorDelegate {
    func productListViewModelShowDetailFor(_ product: Product, viewModel: ProductListViewModel) {
        showDetailFor(product)
    }
}

extension HomeCoordinator: ProductDetailViewModelCoordinatorDelegate {}

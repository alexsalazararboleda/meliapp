//
//  AppCoordinator.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import UIKit

class AppCoordinator: Coordinator {

    // MARK: - Private Properties
    fileprivate var window: UIWindow
    fileprivate var coordinators: CoordinatorsDictionary

    // MARK: - Properties
    var rootViewController: UIViewController {
        let coordinator = coordinators.popFirst()!.1
        return coordinator.rootViewController
    }

    // MARK: - Initializers
    init(window: UIWindow) {
        self.window = window
        coordinators = [:]
    }

    // MARK: - Coordinator
    func start() {
        showHome()
    }

    fileprivate func showHome() {
        let coordinator: HomeCoordinator = {
            $0.delegate = self
            return $0
        }(HomeCoordinator())
        coordinators[HomeCoordinator.name] = coordinator
        window.rootViewController = coordinator.rootViewController
        coordinator.start()
    }
}

extension AppCoordinator: HomeCoordinatorDelegate {}


//
//  Coordinator.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import UIKit

typealias CoordinatorsDictionary = [String: Coordinator]

protocol Coordinator {
    var rootViewController: UIViewController { get }

    func start()
}

extension Coordinator {
    static var name: String {
        return String(describing: self)
    }
}

//
//  ProductListViewController.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import UIKit
import Combine
import EmptyDataSet_Swift

class ProductListViewController: UIViewController, EmptyDataSetSource, EmptyDataSetDelegate  {
    
    @IBOutlet var collectionView: UICollectionView!

    // MARK: - Internal Properties
    var viewModel: ProductListViewModel!
    let searchController = UISearchController(searchResultsController: nil)
    
    private var cancellables = Set<AnyCancellable>()

    // MARK: - Life cycle
    init(viewModel: ProductListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: ProductListViewController.nibName, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "mercado libre"
        self.navigationItem.backButtonTitle = ""
        setupSearchBar()
        setupBindings()
        setupCollectionView()
    }
    
    private func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Busca productos, marcas y más..."
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    private func setupBindings() {
        viewModel.$productList
            .sink {[unowned self] products in
                DispatchQueue.main.async {
                    collectionView.reloadData()
                }
            }
            .store(in: &cancellables)
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewCompositionalLayout {
            (sectionIndex: Int, layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection? in
            let size = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(60))
            let item = NSCollectionLayoutItem(layoutSize: size)
            item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: nil, top: NSCollectionLayoutSpacing.fixed(10), trailing: nil, bottom: NSCollectionLayoutSpacing.fixed(10))
            let group = NSCollectionLayoutGroup.vertical(layoutSize: size, subitems: [item])
            group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 16, bottom: 20, trailing: 16)
            let section = NSCollectionLayoutSection(group: group)
            return section
        }
        
        collectionView.collectionViewLayout = layout
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(ProductViewCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.emptyDataSetDelegate = self
        collectionView.emptyDataSetSource = self
    }
}

extension ProductListViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    viewModel.query = searchController.searchBar.text ?? ""
  }
}

extension ProductListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.productList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductViewCell.nibName, for: indexPath) as? ProductViewCell else { return UICollectionViewCell() }
        let product = viewModel.productList[indexPath.row]
        cell.product = product
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        searchController.searchBar.resignFirstResponder()
        viewModel.handleOpenDetail(at: indexPath.row)
    }
}

extension ProductListViewController {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        if viewModel.query.isEmpty {
            return NSAttributedString(string: "Usa el buscador para encontrar marcas, productos y más...")
        } else {
            return NSAttributedString(string: "No hay publicaciones que coincidan con tu búsqueda")
        }
    }
}

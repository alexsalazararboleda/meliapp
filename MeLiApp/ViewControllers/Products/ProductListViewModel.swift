//
//  ProductListViewModel.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import Foundation
import Combine

protocol ProductListViewModelCoordinatorDelegate: AnyObject {
    func productListViewModelShowDetailFor(_ product: Product, viewModel: ProductListViewModel)
}

class ProductListViewModel {
    // MARK: - Internal properties
    private var cancellables = Set<AnyCancellable>()
    weak var coordinatorDelegate: ProductListViewModelCoordinatorDelegate?
    
    var productRepository: IProductRepository
    
    // MARK: - Internal properties
    @Published var productList: [Product] = []
    @Published var query: String = ""
    
    // MARK: - Initialization functions
    init(_ productRepository: IProductRepository) {
        self.productRepository = productRepository
        setupBindings()
    }
    
    private func setupBindings() {
        $query.debounce(for: 0.5, scheduler: RunLoop.main)
            .removeDuplicates()
            .sink { [unowned self] query in
                search(query)
            }
            .store(in: &cancellables)
    }
    
    private func search(_ query: String) {
        if !query.isEmpty { LoaderView.shared.show() }
        productRepository.searchProductsBy(query: query) { result in
            LoaderView.shared.hide()
            switch result {
            case .success(let list):
                self.productList = list.results
            case .failure(let error):
                self.productList = []
                print(error.localizedDescription)
            }
        }
    }
    
    func handleOpenDetail(at: Int) {
        let product = productList[at]
        coordinatorDelegate?.productListViewModelShowDetailFor(product, viewModel: self)
    }
}

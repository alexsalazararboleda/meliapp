//
//  ProductDetailViewController.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import UIKit
import Combine
import Kingfisher

class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
    
    // MARK: - Internal Properties
    var viewModel: ProductDetailViewModel!
    var cancellables = Set<AnyCancellable>()
    
    // MARK: - Life cycle
    init(viewModel: ProductDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: ProductDetailViewController.nibName, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
    }
    
    private func setupBindings() {
        viewModel.$product
            .map { $0.id }
            .sink(receiveValue: { id in
                self.title = id
            })
            .store(in: &cancellables)
        viewModel.$product
            .map { $0.titleDisplay }
            .assign(to: \.text, on: titleLabel)
            .store(in: &cancellables)
        viewModel.$product
            .map { $0.priceDisplay }
            .assign(to: \.text, on: priceLabel)
            .store(in: &cancellables)
        viewModel.$product
            .map { $0.thumbnail }
            .sink {[unowned self] url in
                guard let url = url else { return }
                imageView.kf.setImage(with: url)
            }
            .store(in: &cancellables)
        viewModel.$product
            .map { $0.availableQuantityDisplay }
            .assign(to: \.text, on: quantityLabel)
            .store(in: &cancellables)
        
    }
}

//
//  ProductDetailViewModel.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import Foundation

protocol ProductDetailViewModelCoordinatorDelegate: AnyObject {}

class ProductDetailViewModel {

    // MARK: - Internal properties
    weak var coordinatorDelegate: ProductDetailViewModelCoordinatorDelegate?

    // MARK: - Internal properties
    @Published var product: Product

    // MARK: - Initialization functions
    init(product: Product) {
        self.product = product
    }
}

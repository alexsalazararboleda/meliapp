//
//  ProductViewCell.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//

import UIKit
import Combine

class ProductViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    var cancellables = Set<AnyCancellable>()
    
    @Published var product: Product?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 8
        setupBindings()
    }
    
    func setupBindings() {
        $product
            .map { $0?.titleDisplay ?? "--" }
            .assign(to: \.text, on: titleLabel)
            .store(in: &cancellables)
        $product
            .map { $0?.priceDisplay ?? "--" }
            .assign(to: \.text, on: priceLabel)
            .store(in: &cancellables)
    }
}

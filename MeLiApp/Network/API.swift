//
//  AppClient.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//

import Foundation
import Moya

// MARK: - Provider setup
public enum API {
    case searchProductBy(String)
}

extension API: TargetType {

    public var baseURL: URL { return URL(string: Constant.baseURL)! }
    public var path: String {
        switch self {
        case .searchProductBy(_):
            return "search"
        }
    }
    public var method: Moya.Method {
        return .get
    }

    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    public var task: Task {
        switch self {
        case .searchProductBy(let query):
            return .requestParameters(parameters: ["q" : query], encoding: URLEncoding.queryString)
//        default:
//            return .requestPlain
        }
    }

    public var validate: Bool {
        switch self {
        default:
            return false
        }
    }

    public var sampleData: Data {
        switch self {
        case .searchProductBy(_):
            return Bundle.main.data(from: "SampleProductList.json")
//        default:
//            return Data()
        }
    }

    public var headers: [String: String]? {
        return nil
    }
}


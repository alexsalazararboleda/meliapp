//
//  AppClient.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//
import Moya

final class AppClient {
    // MARK: - Singleton
    static let shared = AppClient()
    
    static var endpointClosure = { (target: API) -> Endpoint in
        let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
        return defaultEndpoint
    }
    
    static var plugins: [PluginType] {
        #if SAMPLEDATA
        return [NetworkLoggerPlugin(verbose: true)]
        #else
        return []
        #endif
    }
    
    #if SAMPLEDATA
    let provider = MoyaProvider<API>(endpointClosure: endpointClosure, stubClosure: MoyaProvider.immediatelyStub, plugins: plugins)
    #else
    let provider = MoyaProvider<API>(endpointClosure: endpointClosure, plugins: plugins)
    #endif
}

//
//  ProductAPI.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//

import Foundation
import Combine

final class ProductAPI {
    func searchProductsBy(query: String, completion: @escaping (Result<ProductList, Error>) -> ()) {
        NetworkManager.shared.fetchSearchResult(query: query, completion: completion)
    }
}

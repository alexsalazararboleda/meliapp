//
//  NetworkingClient.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//

import Moya

protocol Networkable {
    var provider: MoyaProvider<API> { get }
    func fetchSearchResult(query: String, completion: @escaping (Result<ProductList, Error>) -> ())
}

class NetworkManager: Networkable {
    // MARK: - Singleton
    static let shared = NetworkManager()
    var provider = AppClient.shared.provider
    
    func fetchSearchResult(query: String, completion: @escaping (Result<ProductList, Error>) -> ()) {
        request(target: .searchProductBy(query), completion: completion)
    }
}

private extension NetworkManager {
    private func request<T: Decodable>(target: API, completion: @escaping (Result<T, Error>) -> ()) {
        provider.request(target) { result in
            print(target.baseURL)
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(T.self, from: response.data)
                    completion(.success(results))
                } catch let error {
                    completion(.failure(error))
                }
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}

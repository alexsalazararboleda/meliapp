//
//  Bundle+Decoding.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//

import Foundation

extension Bundle {

    func data(from filename: String) -> Data {
        guard let url = Bundle.main.url(forResource: filename, withExtension: nil) else {
            fatalError("Failed to locate \(filename) in app bundle.")
        }

        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(filename) in app bundle.")
        }
        return data
    }

    func decode<T: Decodable>(_ type: T.Type, from filename: String) -> T {

        let data = self.data(from: filename)
        let decoder = JSONDecoder()

        guard let loaded = try? decoder.decode(type, from: data) else {
            fatalError("Failed to decode \(filename) from app bundle.")
        }
        return loaded
    }
}

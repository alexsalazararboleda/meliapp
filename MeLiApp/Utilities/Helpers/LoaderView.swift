//
//  LoaderView.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/6/21.
//

import SVProgressHUD

class LoaderView {
    
    static let shared = LoaderView()

    func setupUI() {
        SVProgressHUD.setDefaultMaskType(.none)
        SVProgressHUD.setForegroundColor(UIColor(named: "brand")!)
        SVProgressHUD.setBackgroundColor(UIColor.white)
    }

    func show() {
        SVProgressHUD.show()
    }

    func hide() {
        SVProgressHUD.dismiss()
    }
}

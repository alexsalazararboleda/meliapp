//
//  AppearanceHelper.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/6/21.
//

import UIKit
import SVProgressHUD

struct AppearanceHelper {
    static func setup() {
        setupNavigationBar()
        setupProgressHUD()
    }
    
    static fileprivate func setupNavigationBar() {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = UIColor(named: "brand")!
        appearance.titleTextAttributes = [.foregroundColor: UIColor.black]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
        
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 26, weight: .bold)
        ]
        UINavigationBar.appearance().titleTextAttributes = attrs
    }
    
    static fileprivate func setupProgressHUD() {
        LoaderView.shared.setupUI()
    }
}

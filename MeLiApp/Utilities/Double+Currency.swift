//
//  Double+Currency.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//

import Foundation

extension Double {
    func convertDoubleToCurrency() -> String{
        let amount1 = Double(self)
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.maximumFractionDigits = 0
        numberFormatter.locale = Locale(identifier: "en_US")
        return numberFormatter.string(from: NSNumber(value: amount1))!
    }
}

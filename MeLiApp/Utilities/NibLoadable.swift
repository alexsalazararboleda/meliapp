//
//  NibLoadable.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import UIKit

protocol NibLoadable: AnyObject {}

extension NibLoadable where Self: UIViewController {
    static var nibName: String {
        return String(describing: self)
    }
}
extension UIViewController: NibLoadable { }

extension NibLoadable where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}
extension UIView: NibLoadable { }

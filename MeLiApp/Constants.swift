//
//  Constants.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/5/21.
//

import Foundation

struct Constant {
    static let baseURL = "https://api.mercadolibre.com/sites/MLA/"
    static let notAvailable = "https://cdn0.iconfinder.com/data/icons/large-glossy-icons/128/No.png"
}

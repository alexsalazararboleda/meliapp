//
//  Product.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import Foundation

public struct Product: Codable {
    var id: String?
    var title: String?
    var price: Double?
    var availableQuantity: Int?
    var acceptsMercadopago: Bool?
    var permalink: URL?
    var thumbnail: URL?

    enum CodingKeys: String, CodingKey {
        case  id
        case  title
        case  price
        case  availableQuantity = "available_quantity"
        case  acceptsMercadopago = "accepts_mercadopago"
        case  permalink
        case  thumbnail
    }
}

extension Product {
    var titleDisplay: String {
        guard let title = title else { return "" }
        return title
    }
    
    var priceDisplay: String {
        guard let price = price else { return 0.convertDoubleToCurrency() }
        return price.convertDoubleToCurrency()
    }
    
    var availableQuantityDisplay: String {
        guard let quantity = availableQuantity else { return "No disponible" }
        return quantity == 1 ? "\(quantity) disponible" : "\(quantity) disponibles"
    }
}

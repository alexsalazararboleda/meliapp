//
//  ProductList.swift
//  MeLiApp
//
//  Created by iOS Developer on 7/4/21.
//

import Foundation

struct ProductList: Codable {
    var results: [Product]

    enum CodingKeys: String, CodingKey {
        case results = "results"
    }
}
